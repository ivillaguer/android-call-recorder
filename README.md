# Call Recorder

Call recorder with custom recording folder.

No ads, Open Source (GPLv3).

Android-friendly: using system services to record sound and handling API/system calls, and thus no polling or overloading CPU / battery.

MOST PHONES DO NOT SUPPORT CALL RECORDING. Blame Google or your phone manufacturer, not me!

If you have any audio issues (missing the voice of one or both interlocutors): try Encoder / ogg + all sources, then Encoder / aac / Media Recorder + all sources, if you still experince audio issues - then your phone does not support call recording.

If it fails with high quality sound recording (voice line), this app will switch back to the MIC recording.


# Manual install

    gradle installDebug

# Translate

If you want to translate 'Call Recorder' to your language  please read this:

  * [HOWTO-Translate.md](/docs/HOWTO-Translate.md)

# Screenshots

![shot](/docs/shot.png)

# User survey's

Check user surveys at project home page here:

  * [User survey's](https://axet.gitlab.io/android-call-recorder/)
